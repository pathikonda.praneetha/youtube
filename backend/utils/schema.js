import mongoose from "mongoose";

const HOME_VIDEOS_SCHEMA = new mongoose.Schema({
    videourl : String,
    title :String,
    views:String,
    timestamp: String,
    imageurl:String,
    channel:String

});
const TRENDING_VIDEOS_SCHEMA = new mongoose.Schema({
  videourl : String,
    title :String,
    views:String,
    timestamp: String,
    imageurl:String,
    channel:String

});
const WATCHLATER_VIDEOS_SCHEMA = new mongoose.Schema({
    videourl : String,
    title :String,
    views:String,
    timestamp: String,
    imageurl:String,
    channel:String
});

const YOUR_VIDEOS_SCHEMA = new mongoose.Schema({
  videourl : String,
    title :String,
    views:String,
    timestamp: String,
    imageurl:String,
    channel:String
});

const HISTORY_VIDEOS_SCHEMA = new mongoose.Schema({
  videourl : String,
  title :String,
  views:String,
  timestamp: String,
  imageurl:String,
  channel:String
})


const USER_REGISTER_SCHEMA = new mongoose.Schema({
    name: String,
    username: String,
    password: String,
    //re_enter_password: String,
    email: String,
    phone_number: Number,
    security_question: String,
  });
  
  /*
      creating a schema for admin_register_data
      in that withrespected datatypes
  */
  const ADMIN_REGISTER_SCHEMA = new mongoose.Schema({
    name: String,
    username: String,
    password: String,
    //re_enter_password: String,
    email: String,
    phone_number: Number,
    security_question: String,
  });
  
  /* 
      creating a schema for the user_login_data
      withrespected datatypes
  */
  const USER_LOGIN_SCHEMA = new mongoose.Schema({
    username: String,
    password: String,
    jwt_token: String,
    logon_time: Date,
  });
  
  /* 
      creating a schema for the admin_login_data
      withrespected datatypes
  */
  const ADMIN_LOGIN_SCHEMA = new mongoose.Schema({
    username: String,
    password: String,
    jwt_token: String,
    logon_time: Date,
  });
  

export {HOME_VIDEOS_SCHEMA,
    USER_REGISTER_SCHEMA,
    USER_LOGIN_SCHEMA,
    ADMIN_REGISTER_SCHEMA,
    ADMIN_LOGIN_SCHEMA,TRENDING_VIDEOS_SCHEMA,WATCHLATER_VIDEOS_SCHEMA,YOUR_VIDEOS_SCHEMA,HISTORY_VIDEOS_SCHEMA}
