import mongoose from "mongoose";
// import register_data_schema from schema.js file
import {
  USER_REGISTER_SCHEMA,
  USER_LOGIN_SCHEMA,
  ADMIN_LOGIN_SCHEMA,
  ADMIN_REGISTER_SCHEMA
} from "./schema.js";


/* creating a model for the user_lregister_data for creating a list and enter objects in it */
const USER_REGISTER_DATA = mongoose.model(
  "USER_REGISTER_DATA",
  USER_REGISTER_SCHEMA
);

/* creating a model for the user_login_data for storing the user login details */
const USER_LOGIN_DATA = mongoose.model("USER_LOGIN_DATA", USER_LOGIN_SCHEMA);

/* creating a model for the user_login_data for storing the user login details */
const ADMIN_LOGIN_DATA = mongoose.model("ADMIN_LOGIN_DATA", ADMIN_LOGIN_SCHEMA);

/* creating a model for the admin_lregister_data for creating a list and enter objects in it */
const ADMIN_REGISTER_DATA = mongoose.model(
  "ADMIN_REGISTER_DATA",
  ADMIN_REGISTER_SCHEMA
);




/**
 * Inserts the register data into mongoose database by using the 
 * credentials given by the user during registration or signup of the service
 * @param {*} register_data which are user credentials during registration purpose that are 
 *  saved into mongooose database
 */
function inserting_register_data_into_database(register_data) {
  // const USER_LOGIN_data_1 = mongoose.model('USER_LOGIN_data_1', USER_LOGINPAGE_SCHEMA ); // schema
  const user_data = new USER_REGISTER_DATA({
    name: register_data.name,
    username: register_data.username,
    password: register_data.password,
    //re_enter_password: register_data.re_enter_password,
    email: register_data.email,
    phone_number: register_data.phone_no,
    security_question: register_data.security_question,
  });
  // saving the user data into mongoose database
  user_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

/**
 * 
 * @param {*} admin_register_data 
 */
function inserting_admin_register_data_into_database(admin_register_data) {
  // const USER_LOGIN_data_1 = mongoose.model('USER_LOGIN_data_1', USER_LOGINPAGE_SCHEMA ); // schema
  const admin_data = new ADMIN_REGISTER_DATA({
    name: admin_register_data.name,
    username: admin_register_data.username,
    password: admin_register_data.password,
    //re_enter_password: admin_register_data.re_enter_password,
    email: admin_register_data.email,
    phone_number: admin_register_data.phone_no,
    security_question: admin_register_data.security_question,
  });
  // saving the data
  admin_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

/**
 * Saving the credentials given by the user for logging in 
 * @param {*} login_data are user credential for log in purpose which are 
 * used to save data in mongoose database
 */
function inserting_user_login_data_into_database(login_data) {
  var date = new Date()
  login_data.date = date
  const user_login_data = new USER_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  // saving the log in credentials of the user into the mongoose database
  user_login_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

function inserting_admin_login_data_into_database(login_data) {
  var date = new Date()
  login_data.date = date
  const user_login_data = new ADMIN_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  // saving the data
  user_login_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}







/* exporting the inserting_register_data_into_database, 
        inserting_user_login_data_into_database, inserting_admin_register_data_into_database
         functions and USER_REGISTER_DATA, ADMIN_REGISTER_DATA instaces.
    */
export {
  inserting_register_data_into_database,
  USER_REGISTER_DATA,
  ADMIN_REGISTER_DATA,
  USER_LOGIN_DATA,
  ADMIN_LOGIN_DATA,
  inserting_user_login_data_into_database,
  inserting_admin_register_data_into_database,
  inserting_admin_login_data_into_database,
};
