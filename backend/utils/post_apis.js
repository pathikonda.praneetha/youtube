import { request, response } from "express";
import { HOME_VIDEOS_DATA , TRENDING_VIDEOS_DATA,WATCHLATER_VIDEOS_DATA,YOUR_VIDEOS_DATA, HISTORY_VIDEOS_DATA} from "./database.js";


const home_videos_array = []

/**
 * Uploads the video related data for homepage in mongoose
 * @param {*} request video related data as body params
 * @param {*} response when tha data is saved sends response as "video uploaded"
 */
const post_home_videos = (request,response)=>{
    const title = request.body.title
    const videourl = request.body.videourl
    const views =  request.body.views
    const timestamp = request.body.timestamp
    const channel=request.body.channel
    const imageurl=request.body.imageurl

    const post_home_data = {
        title : title,
        videourl: videourl,
        views:views,
        timestamp:timestamp,
        channel:channel,
        imageurl:imageurl
    }

    const posts_home_data = new HOME_VIDEOS_DATA(post_home_data)
    posts_home_data.save().then(()=>response.send('video uploaded'));
    home_videos_array.push(post_home_data)
}


const trending_videos_array = []

/**
 * Uploads the video related data for trendingpage in mongoose
 * @param {*} request video related data as body params
 * @param {*} response when tha data is saved sends response as "video uploaded"
 */
const post_trending_videos = (request,response)=>{
    const title = request.body.title
    const videourl = request.body.videourl
    const views =  request.body.views
    const timestamp = request.body.timestamp
    const channel=request.body.channel
    const imageurl=request.body.imageurl

    const post_trending_data = {
        title : title,
        videourl: videourl,
        views:views,
        timestamp:timestamp,
        channel:channel,
        imageurl:imageurl
    }

    const posts_trending_data = new TRENDING_VIDEOS_DATA(post_trending_data)
    posts_trending_data.save().then(()=>response.send('video uploaded'));
    trending_videos_array.push(post_trending_data)
}

const watchlater_videos_array = []
const post_watchlater_videos = (request,response)=>{
    const title = request.body.title
    const videourl = request.body.videourl
    const views =  request.body.views
    const timestamp = request.body.timestamp
    const channel=request.body.channel
    const imageurl=request.body.imageurl

    const post_watch_later_data = {
        title : title,
        videourl: videourl,
        views:views,
        timestamp:timestamp,
        channel:channel,
        imageurl:imageurl
    }

    const posts_watch_later_data = new WATCHLATER_VIDEOS_DATA(post_watch_later_data)
    posts_watch_later_data.save().then(()=>response.send('video uploaded'));
    watch_later_videos_array.push(post_watch_later_data)
}

const history_videos_array = []
const post_history_videos = (request,response) => {
    const title = request.body.title
    const videourl = request.body.videourl
    const views =  request.body.views
    const timestamp = request.body.timestamp
    const channel=request.body.channel
    const imageurl=request.body.imageurl

    const post_history_data = {
        title : title,
        videourl: videourl,
        views:views,
        timestamp:timestamp,
        channel:channel,
        imageurl:imageurl
    }

    const posts_history_data = new HISTORY_VIDEOS_DATA(post_history_data);
    posts_history_data.save().then(()=> response.send('video uploaded'));
    history_videos_array.push(post_history_data)
}


const your_videos_array = []
const post_your_videos = (request,response)=>{
    const title = request.body.title
    const videourl = request.body.videourl
    const views =  request.body.views
    const timestamp = request.body.timestamp
    const channel=request.body.channel
    const imageurl=request.body.imageurl

    const post_your_videos_data = {
        title : title,
        videourl: videourl,
        views:views,
        timestamp:timestamp,
        channel:channel,
        imageurl:imageurl
    }

    const posts_your_videos_data = new YOUR_VIDEOS_DATA(post_your_videos_data)
    posts_your_videos_data.save().then(()=>response.send('video uploaded'));
    watch_later_videos_array.push(post_your_videos_data)
}

export{ home_videos_array, trending_videos_array,watchlater_videos_array,your_videos_array}
export{post_home_videos,post_trending_videos,post_watchlater_videos,post_your_videos, post_history_videos}
