// Importing mongoose from mongoose
import mongoose from "mongoose";
import {HOME_VIDEOS_SCHEMA, TRENDING_VIDEOS_SCHEMA,WATCHLATER_VIDEOS_SCHEMA,YOUR_VIDEOS_SCHEMA, HISTORY_VIDEOS_SCHEMA} from './schema.js';
import {
  USER_REGISTER_SCHEMA,
  USER_LOGIN_SCHEMA,
  ADMIN_LOGIN_SCHEMA,
  ADMIN_REGISTER_SCHEMA
} from "./schema.js";

// Connecting to the mongoose database
mongoose.connect("mongodb+srv://main-test:Subbareddy@test.jxyoz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");

// Creating model for HOME_VIDEOS_DATA
const HOME_VIDEOS_DATA = mongoose.model("HOME_VIDEOS_DATA",HOME_VIDEOS_SCHEMA);

// Creating model for TRENDING_VIDEOS_DATA
const TRENDING_VIDEOS_DATA = mongoose.model("TRENDING_VIDEOS_DATA",TRENDING_VIDEOS_SCHEMA);
const WATCHLATER_VIDEOS_DATA = mongoose.model("WATCHLATER_VIDEOS_DATA",WATCHLATER_VIDEOS_SCHEMA);
const YOUR_VIDEOS_DATA = mongoose.model("YOUR_VIDEOS_DATA",YOUR_VIDEOS_SCHEMA);

const HISTORY_VIDEOS_DATA = mongoose.model("HISTORY_VIDEOS_DATA",HISTORY_VIDEOS_SCHEMA);

export{HOME_VIDEOS_DATA,TRENDING_VIDEOS_DATA,WATCHLATER_VIDEOS_DATA,YOUR_VIDEOS_DATA,HISTORY_VIDEOS_DATA};
