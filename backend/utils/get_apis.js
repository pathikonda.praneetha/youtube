import { request,response } from "express";
import { HOME_VIDEOS_DATA, TRENDING_VIDEOS_DATA,WATCHLATER_VIDEOS_DATA ,YOUR_VIDEOS_DATA, HISTORY_VIDEOS_DATA} from "./database.js";


// Retrieving video related data for home_page from database (mongoose)
const get_home_videos = (request,response)=>{
    //response.send(JSON.stringify("hello"))
    HOME_VIDEOS_DATA.find({},(error,data)=>{
        if(error){
            console.log(error);
        }
        else{
            response.send(JSON.stringify(data))
        }
    })
}


// Retrieving video related data for trending_page from database (mongoose)
const get_trending_videos = (request,response)=>{
    TRENDING_VIDEOS_DATA.find({},(error,data)=>{
        if(error){
            console.log(error)
        }
        else{
            response.send(data)

        }
    })
}

const get_watchlater_videos = (request,response)=>{
    WATCHLATER_VIDEOS_DATA.find({},(error,data)=>{
        if(error){
            console.log(error)
        }
        else{
            response.send(data)

        }
    })
}

const get_your_videos = (request,response)=>{
    YOUR_VIDEOS_DATA.find({},(error,data)=>{
        if(error){
            console.log(error)
        }
        else{
            response.send(data)

        }
    })
}

const get_history_videos = (request,response) => {
    HISTORY_VIDEOS_DATA.find({},(error,data)=> {
        if(error) {
            console.log(error)
        }
        else{
            response.send(data)
        }
    })
}

export { get_home_videos ,get_watchlater_videos,get_trending_videos,get_your_videos, get_history_videos}
