// defining the conflict_message which contains status_code, status_message in an object
var conflict_message = {
  status_code: 409,
  status_message: "username already exists"
};

/**
 * 
 * @param {1} username it caontains username that are trying to login
 * @returns a object that contains status_code and status_message
 */
function user_alredy_login(username){
return {
  status_code:406,
  status_message: `Already logged in as ${username}`
}
}

// Exporting the conflict_message, user_alredy_login
export
  {
  conflict_message,
  user_alredy_login
  }
