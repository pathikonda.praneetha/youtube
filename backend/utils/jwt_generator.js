// importing jwt from the jsonwebtoken for creating the jwttoken
import jwt from "jsonwebtoken"

/**
 * it generates the jwt_token based on the payload
 * @param username is the username that are passed in login path request body
 * @returns it returns a jwt_token for every function call
 */
function jwt_token_generator(username){
    //initializing the payload for a set of fields that you want to include in the token being generated
    var payload = {
        username:username
    }
    //initializing jwtToken variable for creating jwt Token
    const jwtToken = jwt.sign(payload,"my_secret")
    return jwtToken
}

// exporting the jwt_token_generator function 
export{jwt_token_generator}
