// External imports
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { post_home_videos, post_trending_videos,post_watchlater_videos,post_your_videos, post_history_videos} from './utils/post_apis.js';
import { get_home_videos ,get_watchlater_videos,get_trending_videos,get_your_videos, get_history_videos} from './utils/get_apis.js';
import{ login_user_handler,register_user_handler}from './path_handlers/post_api.js'
import{forgot_user_handler}from './path_handlers/put_api.js'


// Creating an instance of express
const app = express();

// bodyParser.json returns middleware that only parses json.
app.use(bodyParser.json());

// Returns middleware that only parses urlencoded bodies.
app.use(bodyParser.urlencoded({extended:true}));

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.use(cors({origin:"*",}))



//----------------------------Authentication-------------------------------//

// Getting the user data from client and stores it into database
app.post("/register/:user", register_user_handler)

// Validating the user credentials and given authentication
app.post("/login/:user", login_user_handler)

// Validating the user credentials and updating the password
app.put("/forget/:user", forgot_user_handler)


//----------------------------Post Video Content-------------------------//

// Uploading home page content
app.post("/home_videos",post_home_videos)

// Uploading trending page content
app.post("/trending_videos",post_trending_videos)

// uploading music page content 
app.post("/music_videos",post_watchlater_videos)

// uploading history page content
app.post("/history_videos",post_history_videos)

// uploading watchlater page content
app.post("/watchlater_videos",post_watchlater_videos)

app.post("/your_videos",post_your_videos)


//---------------------------Get Video Content---------------------------//

// Retrieving home page content from database (mongoose)
app.get("/home_videos",get_home_videos)

// Retrieving trending page content from database (mongoose)
app.get("/trending_videos",get_trending_videos)

// reteriving music page content
app.get("/music_videos",get_watchlater_videos)

// retrieving history page content 
app.get("/history_videos",get_history_videos);

// retrieving watchlater page content
app.get("/watchlater_videos",get_watchlater_videos)

app.get("/your_videos",get_your_videos)


// Listens to the connections on the specified host and port.
app.listen(1998,()=>{
    console.log("the server is up and running ")
})