//importing functions from the authentication_validation component for validation

import {validate_register_data, validate_user_register_database,
  validate_admin_register_database,validate_user_login_data,validate_admin_login_data,
validate_user_login_data_for_forget_password,validate_admin_login_data_for_forget_password} from "../utils/authentication_validation.js"


  /**
 * @path /register is the path by using this path we can post the user register data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */


const register_user_handler = (request, response) => {
    // register_data is the variable that contains the request body data
  const register_data = request.body;
  // geting user_type from request params
  const user_type = request.params.user;
  // validate_register_data function returns an object that contains the status_code and the status message
  let validation_result = validate_register_data(register_data);
  // if returned result doesn't contains 200 status code the post method returns the error response
  if (validation_result.status_code !== 200) {
    let { status_message } = validation_result;
    // sending the response to the client
    response.send(JSON.stringify(validation_result));
  }
  // if validate_register_data does contains 200 status code it goes to this block
  else {
    // if username already exists in the database it returns some error
    if (user_type === "user") {
      // calling the validate_user_register_database that are defined in the authentication_validation.js
      validate_user_register_database(register_data,response,validation_result)
    } else {
      // calling the validate_admin_register_database that are defined in the authentication_validation.js
      validate_admin_register_database(register_data,response,validation_result)
    }
  }
}




/**
 * @path /login is the path by using this path we can post the user login data and validate it using the register data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
 
const login_user_handler = (request, response) => {
    // login_data is the variable that contains the request body data
    const login_data = request.body;
    // initializing the count is 0 because we need to validate the request parameters
    var count = 0;
    // iteraing over the body data because we need to count the body params
    for (let item in login_data) {
      // updating the count
      count = count + 1;
    }
    // geting user_type from request params
    const user_type = request.params.user_type;
    /*
      we need only username and password for user login
              if any extra params are pass in the request it throws error */
    if (count !== 2) {
      response.send("Please enter valid parameter count");
    }
    // if parameter count are valid execution goes through the else block
    else {
      if(user_type === "user"){
        validate_user_login_data(login_data,response)
      }else{
        validate_admin_login_data(login_data,response)
      }
    }
}
/**
 * @path /forget/user_type is the path by using this path we can validate the user data and update the user/admin password
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */

const forgot_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  // client_key is the jwt token only logedin user are access the restaurent data
  const {client_key} = request.headers
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iteraing over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  // geting user_type from request params
  const user_type = request.params.user_type;
  /*
    we need only username and password for user login
            if any extra params are pass in the request it throws error */
  if (count !== 3) {
    response.send("Please enter valid parameter count");
  }
  // if parameter count are valid execution goes through the else block
  else {
    if(user_type === "user"){
      validate_user_login_data_for_forget_password(login_data,response,client_key)
    }else{
      validate_admin_login_data_for_forget_password(login_data,response,client_key)
    }
  }
}


//exporting register_user_handler and all other api calls to the server.js
export{register_user_handler, login_user_handler, forgot_user_handler}
