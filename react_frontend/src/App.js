import './App.css';
import { Header } from './components/header/Header';
import { Sidebar } from './components/sidebar/Sidebar';
import Homevideos from  './components/homevideos/HomeVideos'
import {BrowserRouter as  Router,Switch,Route} from "react-router-dom";
import SearchPage from './components/searchpage/SearchPage'
import Trendingvideos from './components/trending/Trendingvideos';
import Yourvideos from './components/yourvideos/Yourvideos';
import HistoryVideos from './components/HistoryVideos/HistoryVideos';
import WatchLater from './components/watchlater/WatchLater';
import Library from './components/Library/Library';
import Subscriptions from './components/Subscriptions/Subscriptions';

function App() {
  return (
    <div className="app">
      <Router>
        <Header/>

        <Switch>
        <Route exact path='/'>
            <div className='app_page'>
              <Sidebar/>
              <Homevideos/>
            </div>
          </Route>
        
        <Route exact path='/search/:searchTerm'>
            <div className='app_page'>
              <Sidebar/>
              <SearchPage />
            </div>
          </Route>

    
        <Route exact path='/trending'>
            <div className='app_page'>
              <Sidebar/>
              <Trendingvideos />
            </div>
        </Route>
        <Route exact path='/yourvideos'>
            <div className='app_page'>
              <Sidebar/>
              <Yourvideos />
            </div>
        </Route>

        <Route exact path='/watchlater'>
            <div className='app_page'>
              <Sidebar/>
              <WatchLater />
            </div>
        </Route>

        <Route exact path='/history'>
            <div className='app_page'>
              <Sidebar/>
              <HistoryVideos />
            </div>
        </Route>
        <Route exact path='/library'>
            <div className='app_page'>
              <Sidebar/>
              <Library/>
            </div>
        </Route>
        <Route exact path='/subscriptions'>
            <div className='app_page'>
              <Sidebar/>
              <Subscriptions />
            </div>
        </Route>
          
        </Switch>
        </Router>

      </div>
  )}
export default App;
