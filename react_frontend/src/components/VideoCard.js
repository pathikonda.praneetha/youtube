import React from 'react'
import './VideoCard.css'
import Avatar from "@material-ui/core/Avatar"


//  function VideoCard({video_url,title,views,channel,timestamp,image_url}) {
    const VideoCard = props => {
        const {each} = props
        const {
          videourl,
          imageurl,
          title,
          channel,
          timestamp,
          
          views,
        } = each
    

    return (
        <div className='videoCard'  onClick={ () => console.log("I got clicked") }>
             <iframe
              width={250}
              height={170}
              src={`https://www.youtube.com/embed/${videourl}`} 
              frameBorder={0}
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen="true"
            />
            <div className='VideoCard__info'>
                <Avatar 
                className='videoCard__avatar'
                src={imageurl}
                alt={channel}/>
                <div className='video__text'>
                    <h4>{title}</h4>
                    <p>{channel}</p>
                    <p>{views} || {timestamp}</p>

                </div>
            </div>
        </div>
    )
}

export default VideoCard
