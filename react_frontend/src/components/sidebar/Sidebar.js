import React from 'react'
import './Sidebar.css'
import  {SidebarRow}  from '../sidebarrow/SidebarRow'
import HomeIcon from "@material-ui/icons/Home";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import SubscriptionsIcon from "@material-ui/icons/Subscriptions";
import VideoLibraryIcon from "@material-ui/icons/VideoLibrary";
import HistoryIcon from "@material-ui/icons/History";
import WatchLaterIcon from "@material-ui/icons/WatchLater";
import ExpandMoreOutlinedIcon from "@material-ui/icons/ExpandMoreOutlined";
import OndemandVideoIcon from "@material-ui/icons/OndemandVideo";
import { Link } from 'react-router-dom';








export function Sidebar() {
    

    return (
        <div className='sidebar'>
            <Link to='/' style={{textDecoration: 'none'}}>
            <SidebarRow Icon={HomeIcon} title='Home'/>
            </Link>
            <Link to='/trending' style={{textDecoration:'none'}}> 
            <SidebarRow Icon={WhatshotIcon} title='Trending'/>
            </Link>
            <Link to='/subscriptions' style={{textDecoration:'none'}}> 

            <SidebarRow Icon ={SubscriptionsIcon} title='Subscriptions'/>
            </Link>
            <hr/>
            <Link to='/library' style={{textDecoration:'none'}}> 

            <SidebarRow Icon ={VideoLibraryIcon} title='Library'/>
            </Link>

            <Link to='/history' style={{textDecoration:'none'}}> 
            <SidebarRow Icon ={HistoryIcon} title='History'/>
            </Link>

            <Link to='/yourvideos' style={{textDecoration:'none'}}> 
            <SidebarRow Icon ={OndemandVideoIcon} title='Your videos'/>
            </Link>

            <Link to='/watchlater' style={{textDecoration:'none'}}>
            <SidebarRow Icon ={WatchLaterIcon} title='Watch Later'/>
            </Link>
            
            <SidebarRow Icon ={ExpandMoreOutlinedIcon} title='Show more'/>
            <hr />
           


        </div>
    )
}
