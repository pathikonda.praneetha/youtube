import React, { Component } from 'react'
import './HomeVideos.css'
import VideoCard from '../VideoCard';

class Homevideos extends Component {
    state = {
        videosData :[]
    }

    componentDidMount(){
        this.getServerData()
    }

    getServerData = async()=>{
        const url = "http://localhost:1998/home_videos"
        const option = {
            method:"GET",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }
        const response = await fetch(url,option)
        console.log(response)
        const data = await response.json()
        // console.log(data)
        this.setState({
            videosData:data
        })
        console.log(this.state.videosData)
    }

    render(){
        const {videosData} = this.state
    videosData.sort(()=>Math.random()-0.5)
    return (
        <div className='homevideos'>
            <h2>Recommended</h2> 
           <div className='homeVideos_videos'>
          {videosData.map(each => (
            <VideoCard each={each} key={each.id} />
          ))}
      
    

                {/* <VideoCard 
                title="Pawankalyan"
                views="20.6M Views"
                timestamp="2 days ago"
                channel="Aditya"
                video_url="Hs4NwTd49QA"
                image_url="https://yt3.ggpht.com/a/AGF-l79kfOq8xT6vI_Mp0bCouIQrmGSfKWjVlgkRXA=s900-mo-c-c0xffffffff-rj-k-no"
                /> */}
                {/* <VideoCard 
                title="Virat Kohli"
                views="20.6M Views"
                timestamp="1 year ago"
                channel="BCCI"
                video_url="yFlarM35vxA"
                image_url="https://www.spotnews18.com/wp-content/uploads/2020/04/bcci.jpg"
                />
                <VideoCard 
                title="we'll miss you ABD"
                views="26M Views"
                timestamp="3 months ago"
                channel="RCB"
                video_url="ajBpprey1pI"
                image_url="https://tse2.mm.bing.net/th?id=OIP.4Hot90quQK0OlPatVullbwHaFj&pid=Api&P=0&w=208&h=156"

                />
                <VideoCard 
                title="Kushi telugu movie songs"
                views="5M Views"
                timestamp="9 years ago"
                channel="Aditya"
                video_url="9GjS374jD1c"
                image_url="https://yt3.ggpht.com/a/AGF-l79kfOq8xT6vI_Mp0bCouIQrmGSfKWjVlgkRXA=s900-mo-c-c0xffffffff-rj-k-no"

                />
                <VideoCard 
                title="Radheshyam"
                views="20.6M Views"
                timestamp="2 days ago"
                channel="UV creations"
                video_url="URuqzJ2B8ZM"
                image_url="https://tse2.mm.bing.net/th?id=OIP.F2sIrULkzlmrrJEr_pwzYwHaDt&pid=Api&P=0&w=336&h=168"

                />
                <VideoCard 
                title="RRR || Raja Mouli"
                views="20.6M Views"
                timestamp="2 days ago"
                channel="T-series"
                video_url="Gg7_9Xd06rk"
                image_url="https://tse1.mm.bing.net/th?id=OIP.FIRmZCDnmneVcknWekBfcAHaHa&pid=Api&P=0&w=183&h=183"

                />
                <VideoCard 
                title="Pawankalyan"
                views="20.6M Views"
                timestamp="2 days ago"
                channel="Aditya"
                video_url="Hs4NwTd49QA"
                image_url=""

                />
                <VideoCard 
                title="Pawankalyan"
                views="20.6M Views"
                timestamp="2 days ago"
                channel="Aditya"
                video_url="Hs4NwTd49QA"
                image_url=""

                />
                <VideoCard 
                title="Pawankalyan"
                views="20.6M Views"
                timestamp="2 days ago"
                channel="Aditya"
                video_url="Hs4NwTd49QA"
                image_url=""

                /> */}

           </div>
        </div>
    )
    }
}

export default Homevideos