import React, { useState } from 'react';
import './Header.css';
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import VideoCallIcon from "@material-ui/icons/VideoCall";
import AppsIcon from "@material-ui/icons/Apps";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Avatar from "@material-ui/core/Avatar";
import KeyboardVoiceRoundedIcon from '@material-ui/icons/KeyboardVoiceRounded';
import { Link } from 'react-router-dom';



export function Header() {
    const [inputSearch,setInputSearch] = useState("");

    return (
        <div className="header">
            <div className='header_left'>
            <MenuIcon style={{marginBottom:"15px"}}/>
            <Link to='/'>
            <img className="header__logo" src="http://www.iphonehacks.com/wp-content/uploads/2020/03/YouTube-Logo.jpg" alt="youtube"/>
            </Link>
            </div>

            <div className='header_input'>
            <input onChange={e=>setInputSearch(e.target.value)} value={inputSearch} placeholder="search" type="text"/>
            <KeyboardVoiceRoundedIcon />

            <Link to={`/search/${inputSearch}`}>
            <SearchIcon className='header_search'/> 

            </Link>          
            </div>
            <div>
            </div>
            <div className='header_right'>
            <VideoCallIcon className='header_icon'/>
            <AppsIcon className='header_icon'/>   
            <NotificationsIcon className='header_icon'/> 
            <Avatar alt="Puran Kalapal" src="https://res.cloudinary.com/duwpvypoh/image/upload/v1645741427/20220225_035524_nncqew.jpg"/>
            </div>
            
        </div>
    )
}
