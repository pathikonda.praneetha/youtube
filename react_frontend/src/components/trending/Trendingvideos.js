//importing recat component from react
import React, { Component } from 'react'

// importing css from homevideos.css located in homevideos folder
import '../homevideos/HomeVideos.css'

//importing videocard from other videocard.js from other path
import VideoCard from '../VideoCard';
    
//using class component for defining react component
class Trendingvideos extends Component{
    //using state in to declare to store the property values
    state={
        videosData:[]
    }
    //invoked immediately after a component is mounted
    componentDidMount(){
        this.getServerData()
    }
    //getserverData is the process of getting data from the server 
    getServerData = async()=>{
        const url = "http://localhost:1998/Trending_videos"
        const option = {
            method:"GET",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }
        const response = await fetch(url,option)
        console.log(response)
        const data = await response.json()
        // console.log(data)
        this.setState({
            videosData:data
        })
        console.log(this.state.videosData)
    }
    //rendering the data from the videosdata and to get updated the state 
    render(){
        const {videosData} = this.state
    videosData.sort(()=>Math.random()-0.5)


    return (
        <div className='homevideos'>
            <div className='oriented'>
        <h2>Trending</h2> 
        </div>
        <hr/>
        <div className='homeVideos_videos'>
       {videosData.map(each => (
         <VideoCard each={each} key={each.id} />
       ))}
   </div>
   </div>
    )
}
       }
    
// exporting the trendingvideos
export default Trendingvideos
