import React, { Component } from 'react'
import '../homevideos/HomeVideos.css'
import VideoCard from '../VideoCard';
    
class Library extends Component{
    state={
        videosData:[]
    }
    componentDidMount(){
        this.getServerData()
    }
    getServerData = async()=>{
        const url = "http://localhost:1998/Trending_videos"
        const option = {
            method:"GET",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }
        const response = await fetch(url,option)
        console.log(response)
        const data = await response.json()
        // console.log(data)
        this.setState({
            videosData:data
        })
        console.log(this.state.videosData)
    }
    render(){
        const {videosData} = this.state
    videosData.sort(()=>Math.random()-0.5)


    return (
        <div className='homevideos'>
            <div className='oriented'>
        <h2>Library</h2> 
        </div>
        <hr/>
        <div className='homeVideos_videos'>
       {videosData.map(each => (
         <VideoCard each={each} key={each.id} />
       ))}
   </div>
   </div>
    )
}
       }
    

export default Library
