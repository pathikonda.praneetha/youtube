import React, {Component} from "react";
import '../homevideos/HomeVideos';
import VideoCard from "../VideoCard";


class WatchLater extends Component {
    state = {
        videosData :[]
    }
    
    componentDidMount(){
        this.getServerData()
    }

    getServerData = async()=>{
        const url = "http://localhost:1998/watchlater_videos"
        const option = {
            method:"GET",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }
        const response = await fetch(url,option)
        console.log(response)
        const data = await response.json()
        // console.log(data)
        this.setState({
            videosData:data
        })
        console.log(this.state.videosData)
    }

    render(){
        const {videosData} = this.state

    return (
        <div className='homevideos'>
           <h2 style={{marginBottom:"20px",fontFamily:"Georgia"}}> Watch Later </h2> 
           <div className='homeVideos_videos'>
          {videosData.map(each => (
            <VideoCard each={each} key={each.id} />
          ))}
           </div>
        </div>
    )
    }
}

export default WatchLater;